function paintCell() {
    let target = event.target;
    if (target.tagName === 'TD') {
        target.classList.toggle('yellow');
    }
    let blue = target.closest('TR');
    let blueObj = blue.firstChild;

    if (blueObj.nextSibling === target) {
        blue.classList.toggle('blue');
    }
    if (target.classList.contains('special')) {
        console.log(target);
        target.classList.toggle('green');
    }
}


function startPlay() {
    let basketball = document.querySelector('.basketball');
    let countX = document.querySelector('.page-x');
    let countY = document.querySelector('.page-y');
    let ball = document.querySelector('.ball');
    const two = 2;

    const fieldCoords = event.currentTarget.getBoundingClientRect();
    let ballCoords = {
        top: event.clientY - fieldCoords.top - basketball.clientTop - ball.clientHeight / two,
        left: event.clientX - fieldCoords.left - basketball.clientLeft - ball.clientWidth / two
    };
    if (ballCoords.top < 0) {
        ballCoords.top = 0;
    }

    if (ballCoords.left < 0) {
        ballCoords.left = 0;
    }
    if (ballCoords.left + ball.clientWidth > basketball.clientWidth) {
        ballCoords.left = basketball.clientWidth - ball.clientWidth;
    }

    if (ballCoords.top + ball.clientHeight > basketball.clientHeight) {
        ballCoords.top = basketball.clientHeight - ball.clientHeight;
    }

    ball.style.left = ballCoords.left + 'px';
    ball.style.top = ballCoords.top + 'px';

    if (event.target === countY) {

        let teamA = document.querySelector('.teamA');
        teamA.value++;

    }
    if (event.target === countX) {
        let teamB = document.querySelector('.teamB');
        teamB.value++;
    }
}


function validationPhone() {
    let val = document.getElementById('elem').value;
    let btn = document.getElementById('btn');
    let input = document.getElementById('elem');
    let result = document.getElementById('result');
    let reg = new RegExp(/^\+380[0-9]{9}$/);
    if (reg.test(val)) {
        btn.disabled = false;
        input.classList.add('green-input');
        input.classList.remove('red-input');
        result.innerHTML = `<p class='green-result'>Data was succesfully sent</p>`;

    } else {
        btn.disabled = true;
        input.classList.add('red-input');
        input.classList.remove('green-input');
        result.innerHTML = `<p class='red-result'>Type number does not follow format +380*********</p>`;
    }
}
