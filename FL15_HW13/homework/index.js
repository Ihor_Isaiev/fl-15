const birthday22 = new Date(2000, 9, 22);
const birthday23 = new Date(2000, 9, 23);

function getAge(birthday) {

    let now = Date.now();

    return Math.trunc((now - birthday) / (24 * 3600 * 365.25 * 1000));
}


getAge(birthday22);
getAge(birthday23);

function getWeekDay(date) {
    if (Date.now()) {
        date = new Date();
    }
    let weekDays = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];
    return weekDays[date.getDay()];
}

getWeekDay(Date.now());
getWeekDay(new Date(2020, 9, 22));


function getAmountDaysToNewYear(date) {
    let lastDayOfThisYear = new Date(2020, 11, 31);
    let day = 1000 * 60 * 60 * 24;
    let diff = lastDayOfThisYear - date;

    return Math.ceil(diff / day);

}

getAmountDaysToNewYear(new Date(2020, 7, 30));
getAmountDaysToNewYear(new Date(2020, 0, 1));

function getProgrammersDay(inComeYear) {

    let date = new Date(inComeYear, 0, 1);
    date.setDate(date.getDate() + 255);
    let monthList = ['Yan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    let month = date.getMonth();
    let daysList = ['Sunday', 'Monday', 'Tuesday', ' Wednesday', 'Thursday', 'Friday', 'Saturday'];
    let day = date.getDate();
    let dayWeek = date.getDay();

    return `${day} ${monthList[month]}, ${inComeYear} (${daysList[dayWeek]})`;

}


getProgrammersDay(2020);
getProgrammersDay(2019);


function howFarIs(specifiedWeekday) {
    let days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    let now = new Date();
    let day = now.getDay();
    let weekDayFromValue = specifiedWeekday[0].toUpperCase() + specifiedWeekday.slice(1);
    let indexDay = days.indexOf(weekDayFromValue);

    if (day > indexDay) {
        let dayToDate = indexDay + (days.length - day);
        return `It's ${dayToDate} day(s) left till ${weekDayFromValue}`;
    }
    if (day === indexDay) {
        return `Hey, today is ${weekDayFromValue} =)`;
    }
    if (day < indexDay) {
        let dayToDate = indexDay - day;
        return `It's ${dayToDate} day(s) left till ${weekDayFromValue}`;
    }
}


howFarIs('friday');
howFarIs('Thursday');


function isValidIdentifier(val) {
    let regexValue = /\b^[a-zA-z$_]{1}([{Alpha}$\w+\d+]){0,}$/;
    let value = regexValue.test(val);

    return value;

}


isValidIdentifier('myVar!');
isValidIdentifier('myVar$');
isValidIdentifier('myVar_1');
isValidIdentifier('1_myVar');


const testStr = 'My name is John Smith. I am 27.';

function capitalize(testStr) {
    testStr = testStr.replace(/(^\w{1}|\s{1}\w)/g, testStr => testStr.toUpperCase());
    return testStr;
}

capitalize(testStr);


function isValidAudioFile(audio) {
    let regexValue = /^\b[a-zA-Z]{0,2}[^_][a-zA-Z]{1,}\.(?:flac|mp3|alac|aac)$\b/;
    let value = regexValue.test(audio);

    return value;


}

isValidAudioFile('file.mp4');
isValidAudioFile('my_file.mp3');
isValidAudioFile('file.mp3');


const testString = 'color: #3f3; background-color: #AA00ef; and: #abcd';

function getHexadecimalColors(str) {
    let array = [];
    let regExp = /#([a-f0-9]{3}){1,2}\b/gi;

    if (str.match(regExp) === null) {

        return array;
    }
    return str.match(regExp);

}


getHexadecimalColors(testString);
getHexadecimalColors('red and #0000');


function isValidPassword(val) {

    let regExp = /(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]{8,}/g;

    let password = regExp.test(val);

    return password;
}


isValidPassword('agent007');
isValidPassword('AGENT007');
isValidPassword('AgentOOO');
isValidPassword('Age_007');
isValidPassword('Agent007');


function addThousandsSeparators(val) {

    let newVal = val.toString();

    return newVal.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}


addThousandsSeparators('1234567890');
addThousandsSeparators(1234567890);


const text1 = 'We use https://translate.google.com/ to translate some words and phrases from https://angular.io/ ';
const text2 = 'JavaScript is the best language for beginners!';


function getAllUrlsFromText(text) {

    let array = [];
    let regExp = /(https?:\/\/[^\s]+)/g;

    text = text.match(regExp);

    if (text === null) {
        return array;
    }
    return text;
}

getAllUrlsFromText(text1);
getAllUrlsFromText(text2);
getAllUrlsFromText();

