function reverseNumber(num) {
    let strNum = String(num);
    let result = '';
    let resultMinus = '';
    if (num >= 0) {
        for (let i = strNum.length - 1; i >= 0; i--) {
            result = result + strNum[i];
        }
    }
    if (num < 0) {
        for (let i = strNum.length - 1; i >= 1; i--) {
            resultMinus = resultMinus + strNum[i];
        }
        result = '-' + resultMinus;
    }
    return +result;
}

function forEach(arr, func) {
    let el;
    for (let i = 0; i < arr.length; i++) {
        el = arr[i];
        func(el);
    }

}


function map(arr, func) {
    let result = [];
    forEach(arr, function (arr) {
        result.push(func(arr))
    })
    return result;

}


function filter(arr, func) {
    let arrFilter = [];
    forEach(arr, function (arr) {
        if (func(arr)) {
            arrFilter.push(arr)
        }

    })
    return arrFilter;

}


function getAdultAppleLovers(data) {
    let maxAge = 18;
    let people = [];
    let peopleAge = [];


    filter(data, function (data) {
        if (data['age'] > maxAge) {
            peopleAge.push(data);
        }
    })

    map(peopleAge, function (peopleAge) {
        if (peopleAge['favoriteFruit'] === 'apple') {
            people.push(peopleAge['name']);
        }
    });


    return people;
}


function getKeys(obj) {
    let arr = [];
    for (key in obj) {
        arr.push(key);
    }
    return arr;
}

function getValues(obj) {
    let arr = [];
    for (key in obj) {
        arr.push(obj[key]);
    }
    return arr;
}

function showFormattedDate(dateObj) {
    let montList = ['Yan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    let day = dateObj.getDate();
    let month = dateObj.getMonth();
    let year = dateObj.getFullYear();

    let newDate = `It is ${day} of ${montList[month]}, ${year}`;
    return newDate;
}
