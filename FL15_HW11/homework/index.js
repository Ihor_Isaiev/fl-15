function isEquals(a, b) {
    return a === b;
}

function isBigger(a, b) {
    return a !== b;
}

let storeNames = (...arr) => arr;

function getDifference(a, b) {
    return a > b ? a - b : b - a;
}

function negativeCount(arr) {
    return arr.filter(el => el < 0).length;
}

function letterCount(str, letter) {
    let count = 0;
    for (let i = 0; i < str.length; i++) {
        if (str.charAt(i) === letter) {
            count += 1;
        }
    }
    return count;
}

function countPoints(arr) {
    let count = 0;
    let key;
    let num = 3;


    for (key of arr) {

        let [x, y] = key.split(':');

        if (+x > +y) {
            count = count + num;
        }

        if (+x === +y) {
            count = count + 1;
        }
    }
    return count;
}
