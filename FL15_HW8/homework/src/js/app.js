let nameMeeting = prompt('Please name the meeting', 'meeting');

let form = document.querySelector('form');
let namePerson = document.querySelector('#namePerson');
let time = document.querySelector('#time');
let place = document.querySelector('#place');
let buttonConfirm = document.querySelector('.confirm');
let buttonConvertor = document.querySelector('.convertor');

buttonConfirm.addEventListener('click', validationInput);
buttonConvertor.addEventListener('click', convertor);
form.addEventListener('submit', function (elem) {
    elem.preventDefault()
});

function showForm() {
    if (nameMeeting) {
        form.classList.add('form');
        form.classList.remove('form-hidden');

    }
}

showForm(nameMeeting);

function validationInput() {

    let testTime = /^(2[0-3]|[01]?[0-9]):([0-5]?[0-9])$/;
    let validTime = testTime.test(time.value);

    if (namePerson.value === '' || time.value === '' || place.value === '') {
        alert('Input all data');
    } else if (!validTime) {
        alert(`Enter time in format hh:mm`);
    } else {
        alert(`${namePerson.value} has a ${nameMeeting} today at ${time.value} somewhere in ${place.value}`);
    }
}

function convertor() {
    let receiveEuro = +prompt('Enter the amount of euros');
    let receiveDollar = +prompt('Enter the amount of dollar');
    let euro = 33.52;
    let dollar = 27.76;
    let fixedNumber = 2;
    if (receiveEuro >= 0 && receiveDollar >= 0) {

        let convertEuro = (euro * receiveEuro).toFixed(fixedNumber);
        let convertDollar = (dollar * receiveDollar).toFixed(fixedNumber);

        alert(`${receiveEuro} euros are equal ${convertEuro} hrns,
         ${receiveDollar} dollars are equal ${convertDollar} hrns`);

    } else {
        alert(`Please enter valid values`);
    }

}