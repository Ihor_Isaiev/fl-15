let minMoney = 1000;
let maxPercent = 100;
let minPercent = 0;
let minYears = 1;

function deposit() {
    let years = +prompt('indicate the number of years');
    let percent = +prompt('indicate the percentage of the deposit');
    let amount = +prompt('indicate the amount of money');
    if (years >= minYears
        && Number.isInteger(years)
        && amount >= minMoney
        && percent <= maxPercent
        && percent > minPercent) {
        let result = amount;
        for (let i = 0; i < years; i++) {
            amount = amount + amount * percent / maxPercent;
        }
        let totalAmount = Math.round(amount * maxPercent) / maxPercent;
        let totalProfit = totalAmount - result;
        alert(`Initial amount: ${totalAmount - totalProfit}
Number of years: ${years}
Percentage of year: ${percent}
Total profit: ${totalProfit}
Total amount: ${totalAmount}`)
    } else {
        alert('Invalid input data');
    }
}

deposit();