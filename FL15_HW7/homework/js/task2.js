let count = 3;
let prize = 0;
let totalPrize = 0;
let playNumber = 1;
let range = 9;
let secondRange = 4;
let rangePrizes = 2;

let groupPrizes = {
    firstPrize: 100,
    secondPrize: 50,
    thirdPrize: 25
}
let chance = {
    firstChance: 1,
    secondChance: 2,
    thirdChance: 3
}

function getPlay(totalPrize) {
    let questionBillionaire = confirm('Do you want to play a game?');
    if (!questionBillionaire) {
        return alert('You did not become a billionaire, but can.');
    } else {
        casino(questionBillionaire, totalPrize);
    }
}

getPlay(totalPrize);

function casino(questionBillionaire) {
    while (questionBillionaire) {
        let randomNumber = Math.floor(Math.random() * range);
        console.log(randomNumber);
        for (let i = 1; i <= chance.thirdChance; i++) {
            if (i === chance.firstChance) {
                prize = groupPrizes.firstPrize * playNumber;
            }
            if (i === chance.secondChance) {
                prize = groupPrizes.secondPrize * playNumber;
            }
            if (i === chance.thirdChance) {
                prize = groupPrizes.thirdPrize * playNumber;
            }
            let userNumber = +prompt(`Choose a roulette pocket number from 0 to ${range - 1}
            attempts left: ${count - i}
            Total prize: ${totalPrize}
            Possible prize on current attempt ${prize} `);
            if (userNumber === randomNumber) {
                if (i === chance.firstChance) {
                    alert(`Congratulation, you won! Your prize is: ${prize} $ $. Do you want to continue?`);
                }
                if (i === chance.secondChance) {
                    alert(`Congratulation, you won! Your prize is: ${prize} $ $. Do you want to continue?`);
                }
                if (i === chance.thirdChance) {
                    alert(`Congratulation, you won! Your prize is: ${prize} $ $. Do you want to continue?`);
                }
                totalPrize = totalPrize + prize;
                break;
            } else {
                alert(`Thank you for your participation. Your prize is: ${totalPrize} $`)
            }
        }
        playNumber = playNumber * rangePrizes;
        range = range + secondRange;
        questionBillionaire = false;
        getPlay(totalPrize)
    }
}