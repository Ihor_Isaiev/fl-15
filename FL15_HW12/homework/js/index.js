function visitLink(path) {
    let count = localStorage.getItem(path);
    let value = count ? parseInt(count) : 0;
    value += 1;
    return localStorage.setItem(path, value);

}

function viewResults() {
    document.querySelector('#wt-sky-root').innerHTML = `
  <div>
    <ul>
    <li>You visited Page1 ${localStorage.getItem('Page1')||0} time(s)</li>
    <li>You visited Page2 ${localStorage.getItem('Page2')||0} time(s)</li>
    <li>You visited Page3 ${localStorage.getItem('Page3')||0} time(s)</li>
    </ul>
  </div>
`
    localStorage.clear();
}


